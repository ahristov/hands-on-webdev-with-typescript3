// Weird comparison
console.log(5 == "5"); // JS: true, TS: Error
console.log(5 === "5"); // JS: false, TS: Error
console.log("" === "0"); // JS: false, TS: Error
console.log(0 === ""); // JS: false, TS: Error
console.log({ a: 123 } == { a: 123 }); // JS: false, TS: ?
console.log({ a: 123 } === { a: 123 }); // JS: false, TS: ?
// Bad mutations
var foo = {};
var bar = foo; // bar is a reference to the same object
foo.baz = 123; // JS: OK, TS: Error
console.log(bar.baz); // JS: 123, TS: Error
// Need to handle null and undefined
console.log(undefined == undefined); // true
console.log(null == undefined); // true
var element = document.getElementById("output");
element && (element.innerText = "" + (5 === "5"));
