// Function parameter types

var num: number = 123;
function identity(num: number): number {
  return num;
}
console.log(identity(num));

// Primitive types

var num: number;
var str: string;
var bool: boolean;

num = 123;
num = 123.456;
num = '123'; // TS: Error
console.log(identity(num));

str = '123';
str = 123; // TS: Error

bool = true;
bool = false;
bool = 'false'; // TS: Error


// Typed arrays

var boolArray: boolean[];

boolArray = [true, false];
console.log(boolArray[0]); // true
console.log(boolArray[1]); // false
boolArray[1] = true;
boolArray = [false, true];

boolArray[0] = 'false'; // TS: Error
boolArray = 'false'; // TS: Error
boolArray = [false, 'true']; // TS: Error

// Interfaces

interface Name {
  first: string;
  second: string;
}

var name1: Name;
name1 = {
  first: 'John',
  second: 'Doe'
};

name1 = { // TS: Error `second` is missing
  first: 'John'
};

name1 = {
  first: 'John',
  second: 123, // TS: Error `second` is a number type
};

// Inline type annotation

var nameT: {
  first: string;
  second: string;
};
nameT = {
  first: 'John',
  second: 'Doe'
};
