[] + []; // JavaScript will give you ""
{} + []; // JS: 0, TS: Error
[] + {}; // JS: "[object Object]", TS: Error
{} + {}; // JS: NaN or [object Object][object Object] depending upon browser, TS: Error
"hello" - 1; // JS: NaN, TS: Error

function add(a, b) {
  return
  a + b; // JS: undefined, TS: Error 'unreachable code detected'
}


var element = document.getElementById("output");
element && (element.innerText = `${add(1, 2)}`);