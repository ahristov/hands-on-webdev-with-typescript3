// "Any" type - use to integrate with some 3rd party library
var power: any;

// Takes any and all types
power = 123;
power = 'abc';

// Is compatible with all types

var num: number;
num = power; // Assigns whatever value power has. There is no type checking in this case.
power = num;

console.log('power', power);
console.log('num', num);

// null and anything -- these literals can be assigned to anything

var num: number;
var str: string;

num = null;
console.log('num', num);

num = undefined;
console.log('num', num);

str = null;
console.log('str', str);

str = undefined;
console.log('str', str);

// void - if we want, we can define function that does not return anything as "void". This is optional.
function log(message): void {
  //
}

// Generics

function reverse<T>(items: T[]): T[] {
  var res = [];
  for (let i = items.length - 1; i >= 0; i--) {
    res.push(items[i]);
  }
  return res;
}

var sample=[1,2,3];
var reversed = reverse(sample);
console.log('reversed', reversed);
console.log('reversed', reverse(['a', 'b', 'c']));

// Safety

reversed[0] = '1'; // TS: Error
reversed = ['1', '2']; // TS: Error


// Union type

// Can receive array of string or string as parameter
function formatCommandline(command: string[]|string) {
  var line = '';
  if (typeof command === 'string') {
    line = command.trim();
  } else {
    line = command.join(',').trim();
  }

  console.log('line', line);
}

formatCommandline('Hi union');
formatCommandline(['a', 'b', 'c']);

// Tuple

var nameNumber: [string, number];

nameNumber = ['Jenny', 123];
nameNumber = ['Jenny', '123'];// TS: Error.

console.log(nameNumber);

// Type alias

type StrOrNum = string|number;

var strOrNumber: StrOrNum;
strOrNumber = 123;
strOrNumber = '123';
strOrNumber = false; // TS: Error

console.log('strOrNumber', strOrNumber);