# Hands-On Web Development with TypeScript 3

This repository contains notes and code from studying [Hands-On Web Development with TypeScript 3](https://www.packtpub.com/application-development/hands-web-development-typescript-3-video).

Installing `typescript` globally:

```bash
npm install -g typescript@next
tsc -v
```

See parcel setup example [here](https://github.com/azu/parcel-typescript-example).
